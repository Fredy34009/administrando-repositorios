/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reposiitorio.dao;

import com.repositorio.util.Conexion;
import com.repositorios.modelos.Categoria;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FREDY
 */
public class CategoriaFacade implements Dao<Categoria> {

    Conexion conn=new Conexion();
    PreparedStatement ps;
    ResultSet rs;
    Categoria categoria;

    @Override
    public void create(Categoria entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(Categoria entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Categoria entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Categoria> findAll() {
        try {
            ps = conn.conectar().prepareStatement("select * from categoria");
            rs = ps.executeQuery();
            List<Categoria> lista = new ArrayList<>();
            while (rs.next()) {
                categoria = new Categoria();
                categoria.setIdCategoria(rs.getInt("id_categoria"));
                categoria.setNombre(rs.getString("nombre"));
                categoria.setDescripcion(rs.getString("descripcion"));
                lista.add(categoria);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaFacade.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
