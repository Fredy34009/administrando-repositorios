/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reposiitorio.dao;

import com.repositorio.util.Conexion;
import com.repositorios.modelos.Cliente;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FREDY
 */
public class ClienteFacade implements Dao<Cliente> {

    Conexion conn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;
    Cliente cliente;

    @Override
    public void create(Cliente entity) {
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            ps = conn.conectar().prepareStatement("INSERT INTO cliente(nombre,apellido,direccion, fecha_nacimiento, telefono, email) VALUES (?,?,?,?,?,?)");
            ps.setString(1, entity.getNombre());
            ps.setString(2, entity.getApellido());
            ps.setString(3, entity.getDireccion());
            ps.setString(4, formato.format(entity.getFechaNacimiento()));
            ps.setString(5, entity.getTelefono());
            ps.setString(6, entity.getEmail());
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }

    }

    @Override
    public void edit(Cliente entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Cliente entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cliente> findAll() {

        try {
            ps = conn.conectar().prepareStatement("SELECT * FROM participante26.cliente;");
            rs = ps.executeQuery();
            List<Cliente> lista = new ArrayList<>();
            while (rs.next()) {
                cliente = new Cliente();
                cliente.setIdCliente(rs.getInt("id_cliente"));
                cliente.setNombre(rs.getString("nombre"));
                cliente.setApellido(rs.getString("apellido"));
                cliente.setDireccion(rs.getString("direccion"));
                cliente.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
                cliente.setTelefono(rs.getString("telefono"));
                cliente.setEmail(rs.getString("email"));
                lista.add(cliente);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

}
