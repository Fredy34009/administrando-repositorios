/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reposiitorio.dao;

import com.repositorio.util.Conexion;
import com.repositorios.modelos.Cliente;
import com.repositorios.modelos.Detalle;
import com.repositorios.modelos.Factura;
import com.repositorios.modelos.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FREDY
 */
public class DetalleFacade implements Dao<Detalle> {

    Conexion conn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;
    Detalle detalle;
    Factura factura;
    Producto producto;

    @Override
    public void create(Detalle entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(Detalle entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Detalle entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Detalle> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Detalle> facturas() {
        List<Detalle> list = new ArrayList<>();
        try {
            ps = conn.conectar().prepareStatement("select detalle.num_detalle,factura.num_factura,cliente.id_cliente,cliente.nombre from detalle inner join factura on factura.num_factura=detalle.id_factura inner join cliente on cliente.id_cliente=factura.id_cliente  group by id_factura order by factura.id_cliente");
            rs = ps.executeQuery();
            Cliente cliente;
            while (rs.next()) {
                detalle = new Detalle();
                factura = new Factura();
                producto = new Producto();
                cliente = new Cliente();

                detalle.setNumDetalle(rs.getInt("detalle.num_detalle"));
                factura.setNumFactura(rs.getInt("factura.num_factura"));
                cliente.setIdCliente(rs.getInt("cliente.id_cliente"));
                cliente.setNombre(rs.getString("cliente.nombre"));

                factura.setIdCliente(cliente);
                detalle.setFactura(factura);
                list.add(detalle);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Detalle> productos(Detalle d) {
        List<Detalle> list = new ArrayList<>();
        try {
            System.out.println("id factura "+d.getFactura().getNumFactura());
            ps = conn.conectar().prepareStatement("select detalle.num_detalle,producto.nombre,detalle.precio,producto.precio,detalle.cantidad from detalle inner join producto on producto.id_producto=detalle.id_producto where id_factura=?");
            ps.setInt(1, d.getFactura().getNumFactura());
            rs = ps.executeQuery();
            Cliente cliente;
            while (rs.next()) {
                detalle = new Detalle();
                producto = new Producto();

                detalle.setNumDetalle(rs.getInt("detalle.num_detalle"));
                producto.setNombre(rs.getString("producto.nombre"));
                producto.setPrecio(rs.getDouble("producto.precio"));
                detalle.setIdProducto(producto);
                detalle.setCantidad(rs.getInt("detalle.cantidad"));
                detalle.setPrecio(rs.getDouble("detalle.precio"));
                list.add(detalle);
            }
        } catch (Exception e) {
        }
        return list;
    }

}
