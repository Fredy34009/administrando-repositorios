/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reposiitorio.dao;

import com.repositorio.util.Conexion;
import com.repositorios.modelos.Categoria;
import com.repositorios.modelos.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FREDY
 */
public class ProductoFacade implements Dao<Producto> {

    Conexion conn=new Conexion();
    Producto producto;
    PreparedStatement ps;
    ResultSet rs;

    public ProductoFacade() {
    }

    @Override
    public void create(Producto entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(Producto entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Producto entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Producto> findAll() {
        try {
            ps = conn.conectar().prepareStatement("select * from producto");
            rs = ps.executeQuery();
            List<Producto> lista = new ArrayList<>();
            Categoria cat;
            while (rs.next()) {
                cat = new Categoria();
                cat.setIdCategoria(rs.getInt("id_categoria"));
                producto = new Producto();
                producto.setIdProducto(rs.getInt("id_producto"));
                producto.setNombre(rs.getString("nombre"));
                producto.setPrecio(rs.getDouble("precio"));
                producto.setStock(rs.getInt("stock"));
                producto.setIdCategoria(cat);
                lista.add(producto);
                System.out.println("Tamaño " + lista.size());
            }
            return lista;
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
            return null;
        }
    }

}
