/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.repositorio.control;

import com.reposiitorio.dao.ClienteFacade;
import com.reposiitorio.dao.Dao;
import com.reposiitorio.dao.DetalleFacade;
import com.reposiitorio.dao.FacturaFacade;
import com.reposiitorio.dao.ProductoFacade;
import com.repositorios.modelos.Cliente;
import com.repositorios.modelos.Detalle;
import com.repositorios.modelos.Factura;
import com.repositorios.modelos.ModoPago;
import com.repositorios.modelos.Producto;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author FREDY
 */
@ManagedBean(name = "detalles")
@SessionScoped
public class DetalleController {
    
    private Dao daoCliente,daoProductos,daoFacturas;
    private Cliente cliente;
    private Factura factura;
    private FacturaFacade facturaFacade;
    private List<Cliente> lsClientes;
    private List<Producto> lsProductos;
    private List<Detalle> lsDetalles,lsDetallesProductos;
    private ModoPago modoPago;
    private ClienteFacade clienteFacade;
    private DetalleFacade detalleFacade;
    private Detalle detalle;
    private Double total;

    public DetalleController() {
        daoFacturas=new FacturaFacade();
        daoProductos=new ProductoFacade();
        daoCliente=new ClienteFacade();
        facturaFacade=new FacturaFacade();
        clienteFacade=new ClienteFacade();
        detalleFacade=new DetalleFacade();
        lsClientes=clienteFacade.findAll();
        lsDetalles=detalleFacade.facturas();
        lsProductos=daoProductos.findAll();
        cliente=new Cliente();
        factura=new Factura();
        modoPago=new ModoPago();
        detalle=new Detalle();
        total=0.0;
    }
    public void refresh()
    {
        cliente=new Cliente();
        factura=new Factura();
    }
    public void regCliente()
    {
        try {
            daoCliente.create(cliente);
            refresh();
        } catch (Exception e) {
            System.out.println("Error creando cliente "+e.getMessage());
        }
    }
    public void detalle(Detalle d)
    {
        this.detalle=d;
        try {
            lsDetallesProductos=detalleFacade.productos(detalle);
            total=0.0;
            for(Detalle det: lsDetallesProductos)
            {
               total=det.getPrecio()+total;
            }
        } catch (Exception e) {
        }
    }
    public void facturas(Cliente c)
    {
        try {
               lsDetalles=detalleFacade.facturas();
            System.out.println("Lista "+lsDetalles.size());
        } catch (Exception e) {
            System.out.println("Error facturas"+e.getMessage());
        }
    }
    public void facturar()
    {
        try {
            factura.setIdCliente(cliente);
            factura.setFecha(new Date());
            factura.setNumPago(modoPago);
//            facturaFacade.procedure(factura);
        } catch (Exception e) {
            System.out.println("Errrorrr "+e.getMessage());
        }
    }
    
    //Getter and setter

    public ModoPago getModoPago() {
        return modoPago;
    }

    public void setModoPago(ModoPago modoPago) {
        this.modoPago = modoPago;
    }
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public List<Cliente> getLsClientes() {
        lsClientes=daoCliente.findAll();
        return lsClientes;
    }
    public void setLsClientes(List<Cliente> lsClientes) {
        this.lsClientes = lsClientes;
    }

    public List<Producto> getLsProductos() {
        lsProductos=daoProductos.findAll();
        return lsProductos;
    }

    public void setLsProductos(List<Producto> lsProductos) {
        this.lsProductos = lsProductos;
    }

    public List<Detalle> getLsDetalles() {
        return lsDetalles;
    }

    public void setLsDetalles(List<Detalle> lsDetalles) {
        this.lsDetalles = lsDetalles;
    }

    public List<Detalle> getLsDetallesProductos() {
        return lsDetallesProductos;
    }

    public void setLsDetallesProductos(List<Detalle> lsDetallesProductos) {
        this.lsDetallesProductos = lsDetallesProductos;
    }

    public Detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(Detalle detalle) {
        this.detalle = detalle;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    
    
}
