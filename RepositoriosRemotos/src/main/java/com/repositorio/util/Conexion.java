/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.repositorio.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author FREDY
 */
public class Conexion {

    private Connection conn = null;
    private String db = "participante26";
    private String pass = "root";
    private String user = "root";
    private String url = "jdbc:mysql://localhost:3306/"+db+"?useSSL=false";
    private String driver = "com.mysql.jdbc.Driver";

    public Connection conectar() {
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, pass);
            if (conn != null) {
                System.out.println("Exito");
            }
            return conn;
        } catch (Exception e) {
            return null;
        }

    }

    public static void main(String[] args) {
        Conexion d = new Conexion();
        d.conectar();
    }

}
